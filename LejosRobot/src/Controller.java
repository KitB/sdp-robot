import lejos.nxt.Motor;
import lejos.nxt.NXT;
import lejos.nxt.Sound;

/**
 * Robot Controller which executes commands received from Strategy
 * 
 * @author Iliyan Ivanov
 * 
 */
public class Controller {

	/**
	 * opcode fields
	 */
	private final static int MOVE = 0X01;
	private final static int KICK = 0X02;
	private final static int TURN = 0X03;
	private final static int STOP = 0X04;
	private final static int EXIT = 99;

	private static Receiver receiverThread;
	private static Instinct instinct;

	// The strategy team sends an actual angle in degrees
	private final static double TURN_FACTOR = 1.40;

	public static void main(String[] args) {

		Motor.A.setAcceleration(1000);
		Motor.B.setAcceleration(1000);

		instinct = new Instinct();
		instinct.start();

		receiverThread = new Receiver();
		receiverThread.start();
		
		for (;;) {
			if (receiverThread.hasConnection()) {
				byte[] command = receiverThread.getCommand();
				
				if (command != null) {
					runCommand(command);
				}
			} 
		}
	}

	/**
	 * Checks the opcode and executes the right movement method
	 * 
	 */
	private static void runCommand(byte[] command) {
		int opcode = receiverThread.getOpcode(command);
		switch (opcode) {
		case MOVE:
			int left = receiverThread.getArgument(command, 1);
			int right = receiverThread.getArgument(command, 2);
			setMotorSpeed(left, right);
			break;
		case KICK:
			kick();
			break;
		case TURN:
			byte angle1 = receiverThread.getArgument(command, 1);
			byte angle2 = receiverThread.getArgument(command, 2);
			turnByAngle(joinInt(angle1, angle2));
			break;
		case STOP:
			stop();
			break;
		case EXIT:
			System.out.println("CASE : EXIT");
			quit();
			break;
		}
	}

	/**
	 * Wait for the robot to stop moving, beep, close the receiver thread and
	 * reboot the brick
	 */
	private static void quit() {
		Sound.setVolume(100);
		Sound.beep();
		receiverThread.exit();
		NXT.exit(0);
	}

	/**
	 * Stop any kind of movement - does not wait for the robot to finish its
	 * previous movement
	 */
	private static void stop() {
		Motor.A.setSpeed(0);
		Motor.B.setSpeed(0);
		Motor.A.stop();
		Motor.B.stop();
	}

	/**
	 * Kick once. CAN KICK WHILE MOVING
	 */
	private static void kick() {
		Motor.C.setSpeed(900);
		Motor.C.rotate(-55);
		Motor.C.rotate(55);
	}

	/**
	 * Movement method. Waits for the robot to stop any previous movement Sets
	 * the speeds for the left and the right motor - speeds vary from -90 : 90
	 * negative numbers cause the motors to rotate backwards
	 * 
	 * @param left
	 *            - speed for the left motor (-90;90)
	 * @param right
	 *            - speed for the right motor (-90;90)
	 */
	private static void setMotorSpeed(int left, int right) {
		if (left >= 0 && right >= 0) {
			Motor.A.setSpeed(10 * left);
			Motor.B.setSpeed(10 * right);
			Motor.A.forward();
			Motor.B.forward();
		} else if (left < 0 && right >= 0) {
			Motor.A.setSpeed(10 * (-left));
			Motor.B.setSpeed(10 * right);
			Motor.A.backward();
			Motor.B.forward();
		} else if (left >= 0 && right < 0) {
			Motor.A.setSpeed(10 * left);
			Motor.B.setSpeed(10 * (-right));
			Motor.A.forward();
			Motor.B.backward();
		} else if (left < 0 && right < 0) {
			Motor.A.setSpeed(10 * (-left));
			Motor.B.setSpeed(10 * (-right));
			Motor.A.backward();
			Motor.B.backward();
		}
	}

	/**
	 * Turn the robot by the given angle. - again waits for any previous
	 * movement to finish
	 * 
	 * @param angle
	 *            - the angle to turn by in degrees
	 */
	private static void turnByAngle(int angle) {
		stop();

		System.out.println("Turning by " + angle);
		Motor.A.setSpeed(400);
		Motor.B.setSpeed(400);
		int angleNew = (int) (angle * TURN_FACTOR);
		Motor.A.rotate(-angleNew, true);
		Motor.B.rotate(angleNew, true);
		while (Motor.A.isMoving() && Motor.B.isMoving()) {
			; // wait for previous movement to finish
		}
	}

	/**
	 * Joins the two received bytes into an int
	 */
	public static int joinInt(byte x, byte y) {
		int a = x << 8;
		int b = y & 255;
		System.out.println(a + " + " + b);
		return a + b;
	}

	/**
	 * Robot move forward/backward for a certain distance
	 * 
	 * @param speed
	 *            - speed of robot between -7 : +7
	 * @param distance
	 *            - distance we want to travel in ~cm~
	 */
	public static void travel(int speed, int distance) {
		Motor.A.setSpeed(100 * speed);
		Motor.B.setSpeed(100 * speed);
		int go = 14 * distance;
		if (speed < 0) {
			Motor.A.rotate(-go, true);
			Motor.B.rotate(-go, true);
		} else {
			Motor.A.rotate(go, true);
			Motor.B.rotate(go, true);
		}
	}
}
