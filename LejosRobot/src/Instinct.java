import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;

public class Instinct extends Thread {

	private static final int REACTION_TIME = 50;

	private TouchSensor leftTouch = new TouchSensor(SensorPort.S2);
	private TouchSensor rightTouch = new TouchSensor(SensorPort.S1);

	/**
	 * Back away if any of the touch sensors are pressed
	 */
	public void run() {
		for (;;) {
			while (leftTouch.isPressed() || rightTouch.isPressed()) {
				Controller.travel(-20, 15); // Move back a little
			}

			// Right now strategy don't care about this
			// receiverThread.sendTouch();

			try {
				Thread.sleep(REACTION_TIME);
			} catch (InterruptedException e) {
				System.err.println("Instinct: Sleep interrupted: "
						+ e.toString());
			}
		}
	}
}
