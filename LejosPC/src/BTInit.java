import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lejos.pc.comm.NXTComm;
import lejos.pc.comm.NXTCommException;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTInfo;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

public class BTInit
{
	public static final String NXT_MAC_ADDRESS = "00:16:53:08:A0:E6";
	public static final String NXT_NAME = "group6";
	
	private static InputStream in;
	private static OutputStream out;
	
	public static void main(String args[]) throws IOException, InterruptedException 
	{
		
		
		System.out.println("PC: >> Initiate Bluetooth connection");
		NXTInfo nxtInfo = new NXTInfo(NXTCommFactory.BLUETOOTH, "NXT",
			NXT_MAC_ADDRESS);

		try {
			NXTComm nxtComm = NXTCommFactory.createNXTComm(
				NXTCommFactory.BLUETOOTH);
			nxtComm.open(nxtInfo);
			
			in = nxtComm.getInputStream();
			out = nxtComm.getOutputStream();
			
			System.out.println("PC: >> Connection established!");
			
			System.out.println("PC: >> Writing to output stream...");
			System.out.println("PC: >> Sending byte: 0xFF");
			
			JFrame frame = new JFrame("Key Listener");
		    Container contentPane = frame.getContentPane();
		    			
			KeyListener listener = new KeyListener() {
			      public void keyPressed(KeyEvent e) {
			    	  if(e.getKeyCode() == KeyEvent.VK_W){
			    		  System.out.println("Forward");			    		  
			    		  sendCommand(0x01);			    		  
			    	  } else if(e.getKeyCode() == KeyEvent.VK_S){
			    		  System.out.println("Stop");
			    		  sendCommand(0x02);			    		  
			    	  } else if(e.getKeyCode() == KeyEvent.VK_A){
			    		  System.out.println("Left");
			    		  sendCommand(0x04);
			    	  } else if(e.getKeyCode() == KeyEvent.VK_D){
			    		  System.out.println("Right");
			    		  sendCommand(0x05);
			    	  } else if(e.getKeyCode() == KeyEvent.VK_K){
			    		  sendCommand(0x07);
			    	  } else if(e.getKeyCode() == KeyEvent.VK_P){
			    		  sendCommand(0x08);
			    	  } else if(e.getKeyCode() == KeyEvent.VK_Q){
			    		  System.out.println("Rotate 90 Left");
			    		  sendCommand(0x09);
			    	  } else if(e.getKeyCode() == KeyEvent.VK_E){
			    		  System.out.println("Rotate 90 Right");
			    		  sendCommand(0x10);
			    	  }
			      }

			      public void keyReleased(KeyEvent e) {
			    	  if(e.getKeyCode() == KeyEvent.VK_W){
			    		  System.out.println("Stop");
			    		  sendCommand(0x03);			    		  
			    	  } else if(e.getKeyCode() == KeyEvent.VK_A){
			    		  System.out.println("Straight");
			    		  sendCommand(0x06);
			    	  } else if(e.getKeyCode() == KeyEvent.VK_D){
			    		  System.out.println("Straight");
			    		  sendCommand(0x06);
			    	  } else if(e.getKeyCode() == KeyEvent.VK_S){
			    		  System.out.println("Stop");
			    		  sendCommand(0x03);
			    	  } else if(e.getKeyCode() == KeyEvent.VK_Q){
			    		  System.out.println("Stop");
			    		  sendCommand(0x03);
			    	  } else if(e.getKeyCode() == KeyEvent.VK_E){
			    		  System.out.println("Stop");
			    		  sendCommand(0x03);
			    	  } 

			      }

				@Override
				public void keyTyped(KeyEvent e) {
					// TODO Auto-generated method stub
					//if(e.getKeyCode() == KeyEvent.VK_Q){
					//	sendCommand(0x09);
					//}
					
				}    
			      
			    };
			    
			    JTextField text = new JTextField();
			    text.addKeyListener(listener);
			    contentPane.add(text, BorderLayout.NORTH);
			    frame.pack();
			    frame.show();
			    
		} catch (NXTCommException e) {
			throw new IOException("Failed to connect " 
				+ e.toString());
		}
		
		in.close();
		out.close();
	}
	
	static void sendCommand(int opcode){
		try {	
			out.write(opcode);
		  } catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		  }
		   	  
		  try {
			out.flush();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
  	 
	}
	
}
